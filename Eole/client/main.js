import { Meteor } from "meteor/meteor";
import Chart from "chart.js";

Meteor.startup(() => {});

//-----------------------------------------------------------------Définition des variables

const PathAcedemie = document.querySelectorAll(".Academie>path");
const DOMTOM = document.querySelectorAll(".DOMTOM");
const BoutonModeSelectorAcad = document.querySelector("#radio-acad");
const BoutonModeSelectorReg = document.querySelector("#radio-reg");
const BoutonModeSelectorFr = document.querySelector("#radio-fr");
const NomSelection = document.querySelector("h2");
const Soulignage = document.querySelector("#Soulignage");

var SelectionMode = "Region";
var StatistiquesAffiche = false;
var NomClick;
var result = {};
var tabModule = [];
var TabLabelsActif = [];
var TabLabelsActif2 = [];
var TabResultNonNul = [];
var TabPieChart = [];
var TabPieChartLabel = [];
var TabTotalHover = [];

//----------------------------------------------------------------Création du spinner

import { Spinner } from "spin.js";

var opts = {
  lines: 11, // The number of lines to draw
  length: 38, // The length of each line
  width: 17, // The line thickness
  radius: 45, // The radius of the inner circle
  scale: 1, // Scales overall size of the spinner
  corners: 1, // Corner roundness (0..1)
  speed: 1, // Rounds per second
  rotate: 0, // The rotation offset
  animation: "spinner-line-fade-quick", // The CSS animation name for the lines
  direction: 1, // 1: clockwise, -1: counterclockwise
  color: "#000f0f", // CSS color or array of colors
  fadeColor: "transparent", // CSS color or array of colors
  top: "50%", // Top position relative to parent
  left: "50%", // Left position relative to parent
  shadow: "0 0 1px transparent", // Box-shadow for the lines
  zIndex: 2000000000, // The z-index (defaults to 2e9)
  className: "spinner", // The CSS class to assign to the spinner
  position: "absolute", // Element positioning
};

var spinner = new Spinner(opts);

//---------------------------------------------------------Changement du mode de selection
BoutonModeSelectorAcad.addEventListener("click", function () {
  SelectionMode = "Academie";
});
BoutonModeSelectorReg.addEventListener("click", function () {
  SelectionMode = "Region";
});
BoutonModeSelectorFr.addEventListener("click", function () {
  SelectionMode = "Fr";
  NomClick = "France Total";
  NomSelection.textContent = NomClick + " :";
  AffichageStats();
});

//Création des graphiques
var ctx = document.getElementById("myChart").getContext("2d");
var myChart = new Chart(ctx, {
  type: "bar",
  data: {
    labels: tabModule,
    datasets: [
      {
        label: "Modules actifs dans les 2 derniers mois",
        data: result[1],
        backgroundColor: [],
        borderColor: [],
        borderWidth: 1,
      },
    ],
  },
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});

var ctx2 = document.getElementById("PieChart").getContext("2d");
var myChart2 = new Chart(ctx2, {
  type: "pie",
  data: {
    labels: {
      text: TabPieChartLabel,
    },
    datasets: [
      {
        label: "Modules les plus actifs :",
        data: TabPieChart,
        backgroundColor: [
          "rgba(255, 99, 132, 0.8)",
          "rgba(54, 162, 235, 0.8)",
          "rgba(255, 206, 86, 0.8)",
          "rgba(153, 102, 255, 0.8)",
          "rgba(255, 159, 64, 0.8)",
        ],
        borderColor: [
          "rgba(255, 99, 132, 1)",
          "rgba(54, 162, 235, 1)",
          "rgba(255, 206, 86, 1)",
          "rgba(153, 102, 255, 1)",
          "rgba(255, 159, 64, 1)",
        ],
        borderWidth: 1,
      },
    ],
  },
  options: {
    legend: {
      labels: {
        fontColor: "#3b2f68",
        fontSize: 18,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  },
});

//Javascript sur ordinateur
if (window.innerWidth > 600) {
  spinner.spin(document.querySelector("body"));
  Meteor.call("RequeteStart", function (error, result) {
    TabTotalHover = result;
    spinner.stop();
  });

  for (let i = 0; i < PathAcedemie.length; i++) {
    //Gestion du Hover des académie et de l'affichage du Nom de cette dernière
    PathAcedemie[i].addEventListener("mouseenter", function () {
      if (StatistiquesAffiche == false) {
        if (SelectionMode == "Academie") {
          this.style.fill = "#c7c7c7";
          NomSelection.textContent = this.id + " :";
          Soulignage.style.opacity = "1";
          Soulignage.style.width = this.id.length * 2 + "vmin";

          if (TabTotalHover[this.id][0]) {
            NomSelection.textContent += "(" + TabTotalHover[this.id][0] + ")";
          }
        } else {
          RegionStr = this.className.animVal;
          RegionPath = document.getElementsByClassName(RegionStr);
          for (let i = 0; i < RegionPath.length; i++) {
            RegionPath[i].style.fill = "#c7c7c7";
            NomSelection.textContent = this.className.animVal + " :";
            Soulignage.style.opacity = "1.0";
            Soulignage.style.width = this.className.animVal.length * 2 + "vmin";
          }
          if (TabTotalHover[this.className.animVal][0]) {
            NomSelection.textContent +=
              "(" + TabTotalHover[this.className.animVal][0] + ")";
          }
        }
      }
    });

    PathAcedemie[i].addEventListener("mouseleave", function () {
      if (SelectionMode == "Academie") {
        this.style.fill = "#8f89b1";
      } else {
        RegionPath = document.getElementsByClassName(RegionStr);
        for (let i = 0; i < RegionPath.length; i++) {
          RegionPath[i].style.fill = "#8f89b1";
        }
      }
    });

    //Lance l'affichage des statistiques
    PathAcedemie[i].addEventListener("click", function () {
      if (SelectionMode == "Academie") {
        NomClick = this.id;
      } else {
        RegionStr = this.className.animVal;
        RegionPath = document.getElementsByClassName(RegionStr);
        for (let i = 0; i < RegionPath.length; i++) {
          NomClick = this.className.animVal;
        }
      }

      ElementClick = this;
      AffichageStats(ElementClick);
    });
  }

  for (let i = 0; i < DOMTOM.length; i++) {
    DOMTOM[i].addEventListener("mouseenter", function () {
      if (StatistiquesAffiche == false) {
        this.style.fill = "#c7c7c7";
        NomSelection.textContent = this.id + " :";
        Soulignage.style.opacity = "1";
        Soulignage.style.width = this.id.length * 2 + "vmin";

        if (TabTotalHover[this.id]) {
          NomSelection.textContent += "(" + TabTotalHover[this.id][0] + ")";
        }
      }
    });

    DOMTOM[i].addEventListener("mouseleave", function () {
      this.style.fill = "#8f89b1";
    });

    DOMTOM[i].addEventListener("click", function () {
      NomClick = this.id;
      ElementClick = this;
      AffichageStats(ElementClick);
    });
  }
}

//Javascript sur mobile
else if (window.innerWidth < 700) {
  for (let i = 0; i < PathAcedemie.length; i++) {
    PathAcedemie[i].addEventListener("mouseenter", function () {
      if (StatistiquesAffiche == false) {
        if (SelectionMode == "Academie") {
          this.style.fill = "#c7c7c7";
          NomSelection.textContent = this.id + " :";
          Soulignage.style.opacity = "1";
          Soulignage.style.width = this.id.length * 2 + "vmin";
        } else {
          RegionStr = this.className.animVal;
          RegionPath = document.getElementsByClassName(RegionStr);
          for (let i = 0; i < RegionPath.length; i++) {
            RegionPath[i].style.fill = "#c7c7c7";
            NomSelection.textContent = this.className.animVal + " :";
            Soulignage.style.opacity = "1.0";
            Soulignage.style.width = this.className.animVal.length * 2 + "vmin";
          }
        }
      }
    });

    PathAcedemie[i].addEventListener("mouseleave", function () {
      if (SelectionMode == "Academie") {
        this.style.fill = "#8f89b1";
      } else {
        RegionPath = document.getElementsByClassName(RegionStr);
        for (let i = 0; i < RegionPath.length; i++) {
          RegionPath[i].style.fill = "#8f89b1";
        }
      }
    });

    PathAcedemie[i].addEventListener("click", function () {
      if (SelectionMode == "Academie") {
        NomClick = this.id;
      } else {
        RegionStr = this.className.animVal;
        RegionPath = document.getElementsByClassName(RegionStr);
        for (let i = 0; i < RegionPath.length; i++) {
          NomClick = this.className.animVal;
        }
      }

      ElementClick = this;
    });
  }

  for (let i = 0; i < DOMTOM.length; i++) {
    DOMTOM[i].addEventListener("mouseenter", function () {
      if (StatistiquesAffiche == false) {
        this.style.fill = "#c7c7c7";
        NomSelection.textContent = this.id + " :";
        Soulignage.style.opacity = "1";
        Soulignage.style.width = this.id.length * 2 + "vmin";
      }
    });

    DOMTOM[i].addEventListener("mouseleave", function () {
      this.style.fill = "#8f89b1";
    });

    DOMTOM[i].addEventListener("click", function () {
      NomClick = this.id;
      ElementClick = this;
    });
  }

  document.querySelector("button").addEventListener("click", function () {
    if (NomSelection.textContent != "") {
      AffichageStats(ElementClick);
    }
  });
}

//Function pour ouvrir l'onglet statistiques
function AffichageStats() {
  document.querySelector("#PieDiv").style.opacity = 1;

  spinner.spin(document.querySelector("#Statistiques"));
  Meteor.call("Requete", function (error, result) {
    spinner.stop();
    tabModule = result[1];

    TabTypeModule = [];

    tabModule.forEach((nomModule) => {
      TabTypeModule.push(nomModule.split("-")[0]);
    });

    const ModuleFiltered = TabTypeModule.filter((item, index) => {
      return TabTypeModule.indexOf(item) === index;
    });

    document.querySelector("#SpanTotal").textContent = result[0][NomClick][0];
    TabLabelsActif = [];
    TabResultNonNul = [];
    for (var i = 0; i < result[1].length; i++) {
      if (result[0][NomClick][1][result[1][i]] > 0) {
        TabLabelsActif.push(i);
        TabResultNonNul.push(result[0][NomClick][1][result[1][i]]);
      }
    }

    for (var i = 0; i < TabLabelsActif.length; i++) {
      TabLabelsActif[i] = tabModule[TabLabelsActif[i]];
    }

    ColorTab = [
      "rgba(210, 41, 46, 0.7)",
      "rgba(155, 35, 240, 0.7)",
      "rgba(71, 146, 64, 0.7)",
      "rgba(240, 130, 32, 0.7)",
      "rgba(240, 190, 32, 0.7)",
      "rgba(5, 145, 200, 0.7)",
    ];
    ModuleColorTab = [];
    lastColor = 0;
    indexColor = 0;
    for (let i = 0; i < TabResultNonNul.length; i++) {
      color = ModuleFiltered.indexOf(TabLabelsActif[i].split("-")[0]) * 255;
      if (lastColor == color) {
        ModuleColorTab[i] = ColorTab[indexColor];
      } else {
        indexColor++;
        if (!ColorTab[indexColor]) {
          console.log("PROBLEMOS");
          indexColor = 0;
        }
        ModuleColorTab[i] = ColorTab[indexColor];
      }
      lastColor = color;
      console.log(ModuleColorTab);
    }

    ModuleFiltered.forEach((typeModule) => {});
    myChart.data.datasets[0].data = TabResultNonNul;
    myChart.data.labels = TabLabelsActif;
    myChart.data.datasets[0].backgroundColor = ModuleColorTab;
    myChart.update();

    autre = 0;
    TabPieChart = [];
    TabPieChartLabel = [];
    TabResltNonNulSplice = [];

    for (var i = 0; i < TabResultNonNul.length; i++) {
      TabResltNonNulSplice.push(TabResultNonNul[i]);
    }

    TabLabelsActif2 = [...TabLabelsActif];
    for (var i = 0; i < 4; i++) {
      TabPieChart.push(Math.max.apply(null, TabResltNonNulSplice));
      TabPieChartLabel.push(
        TabLabelsActif2[
          TabResltNonNulSplice.indexOf(
            Math.max.apply(null, TabResltNonNulSplice)
          )
        ]
      );
      TabLabelsActif2.splice(
        TabResltNonNulSplice.indexOf(
          Math.max.apply(null, TabResltNonNulSplice)
        ),
        1
      );
      TabResltNonNulSplice.splice(
        TabResltNonNulSplice.indexOf(
          Math.max.apply(null, TabResltNonNulSplice)
        ),
        1
      );
    }
    for (var i = 0; i < TabResltNonNulSplice.length; i++) {
      autre += TabResltNonNulSplice[i];
    }
    TabPieChart.push(autre);
    TabPieChartLabel.push("autre");

    for (var i = 0; i < 5; i++) {
      if (TabPieChart[i] >= 0) {
      } else {
        TabPieChart.splice(i, 1);
        TabPieChartLabel.splice(i, 1);
      }
    }

    myChart2.data.datasets[0].data = TabPieChart;
    myChart2.data.labels = TabPieChartLabel;
    myChart2.update();
  });

  StatistiquesAffiche = true;

  if (window.innerWidth > 600) {
    const TransStatG = anime({
      targets: "#Statistiques",
      translateX: -window.innerWidth / 1.6,
      easing: "easeInOutSine",
      duration: 1000,
    });

    const FadeOutDOM = anime({
      targets: "#DOM-TOM",
      opacity: 0,
      easing: "easeInOutSine",
      duration: 800,
      complete: function () {
        const FadeInClicked = anime({
          targets: "#AcademieClick",
          opacity: 1,
          duration: 1500,
        });
      },
    });
  } else if (window.innerWidth < 600) {
    const TransStatG = anime({
      targets: "#Statistiques",
      translateY: [0, -window.innerHeight / 1.2],
      easing: "easeInOutSine",
    });
  }
}

//Function pour fermer l'onglet statistiques
document.querySelector("#FermerStats").addEventListener("click", function () {
  if (NomClick == "France Total") {
    BoutonModeSelectorReg.checked = true;
  }

  document.querySelector("#PieDiv").style.opacity = 0;

  myChart2.data.datasets[0].data = [];
  myChart2.data.labels = [];
  myChart2.update();

  myChart.data.datasets[0].data = [];
  myChart.data.labels = [];
  myChart.update();

  document.querySelector("#SpanTotal").textContent = "";

  StatistiquesAffiche = false;
  const TransStatG = anime({
    targets: "#Statistiques",
    translateX: 0,
    translateY: 0,
    easing: "easeInOutSine",
    duration: 1000,
  });
  anime({
    targets: "#DOM-TOM",
    opacity: 1,
    easing: "easeInOutSine",
    duration: 800,
  });
});
