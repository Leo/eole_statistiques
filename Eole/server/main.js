import { Meteor } from "meteor/meteor";
import { Mongo } from "meteor/mongo";
import { _ } from "lodash";

var total, Count;
var db;
var tabModule = [];

//Tableau contenant tout les département de chaques académies
var DepartementAcad = {
  "Aix-Marseille": ["13", "84", "4", "5"],
  Toulouse: ["65", "32", "31", "9", "82", "81", "12", "46"],
  Rennes: ["29", "22", "56", "35"],
  Caen: ["50", "14", "61"],
  Rouen: ["76", "27"],
  Nantes: ["44", "85", "49", "53", "72"],
  Poitiers: ["17", "79", "86", "16"],
  Bordeaux: ["24", "33", "40", "47", "64"],
  Montpellier: ["66", "11", "34", "30", "48"],
  "Orleans-Tour": ["28", "45", "41", "18", "36", "37"],
  Versailles: ["78", "91", "92", "95"],
  Paris: ["75"],
  Créteil: ["77", "93", "94"],
  Amiens: ["2", "60", "80"],
  Lille: ["59", "62"],
  Limoges: ["87", "23", "19"],
  "Clermond-Ferrand": ["3", "63", "15", "43"],
  Grenoble: ["74", "73", "38", "7", "26"],
  Nice: ["6", "83"],
  Corse: ["620", "720"],
  Lyon: ["1", "69", "42"],
  Dijon: ["21", "71", "58", "89"],
  Besançon: ["70", "25", "39", "90"],
  Reims: ["52", "10", "51", "8"],
  "Nancy-Mets": ["55", "54", "57", "88"],
  Strasbourg: ["67", "68"],
  "Polynésie Francaise": ["984"],
  Martinique: ["972"],
  Mayotte: ["976"],
  "Nouvelle Calédonie": ["983"],
  "St Pierre et Miquelon": ["975"],
  "Wallis et Futuna": ["986"],
  Reunion: ["974"],
  Guyane: ["973"],
  Guadeloupe: ["971"],
  Bretagne: ["29", "22", "56", "35"],
  "Pays de la Loire": ["44", "85", "49", "53", "72"],
  "Centre-Val de Loire": ["28", "45", "41", "18", "36", "37"],
  Normandie: ["50", "14", "61", "76", "27"],
  "Nouvelle-Aquitaine": [
    "24",
    "33",
    "40",
    "47",
    "64",
    "17",
    "79",
    "86",
    "16",
    "87",
    "23",
    "19",
  ],
  Occitanie: [
    "65",
    "32",
    "31",
    "9",
    "82",
    "81",
    "12",
    "46",
    "66",
    "11",
    "34",
    "30",
    "48",
  ],
  "Provence-Alpes-Côte D'Azur": ["13", "84", "4", "5", "6", "83"],
  "Auvergne-Rhône-Alpes": [
    "74",
    "73",
    "38",
    "7",
    "26",
    "1",
    "69",
    "42",
    "3",
    "63",
    "15",
    "43",
  ],
  "Bourgogne-Franche-Comté": ["21", "71", "58", "89", "70", "25", "39", "90"],
  "Île-de-France": ["78", "91", "92", "95", "77", "93", "94", "75"],
  "Hauts-de-France": ["2", "60", "80", "59", "62"],
  "Grand Est": ["52", "10", "51", "8", "55", "54", "57", "88", "67", "68"],
};

//Tableau contenant toute les selections possible ( académies et regions académiques)
var SelectionPossible = [];

//Calcul de la date minimale pour concidéré qu'un module est actif ( date d'aujourd'hui moins deux mois)
var DateActu = new Date();
var DateLess = DateActu - 5256000000;
var DateLast = new Date(DateLess);
var DateLastString =
  DateLast.getFullYear() + "-0" + DateLast.getMonth() + "-" + DateLast.getDay();

//Lancement de meteor
Meteor.startup(() => {
  //Créer la collection mongoDB
  const Eole = new Mongo.Collection("Eole");
  Eole.rawCollection().createIndex({ dep: -1 });

  for (const selec in DepartementAcad) {
    SelectionPossible.push(`${selec}`);
  }

  var tabModule = distinct(Eole, "module");
  function distinct(collection, field) {
    return _.uniq(
      collection
        .find(
          {},
          {
            sort: { [field]: 1 },
            fields: { [field]: 1 },
          }
        )
        .fetch()
        .map((x) => x[field]),
      true
    );
  }

  let fullTab = {};
  let tabModuleSelec = {};
  let TabNBModules;

  SelectionPossible.forEach((selectPoss) => {
    countModulesActifs = 0;

    TabNBModules = {};
    tabModule.forEach((module) => {
      TabNBModules[module] = 0;
    });

    DepartementAcad[selectPoss].forEach((dep) => {
      countModulesActifs += Eole.find({ dep: parseInt(dep, 10) }).count();
      tabModule.forEach((module) => {
        TabNBModules[module] += Eole.find({
          dep: parseInt(dep, 10),
          module: module,
        }).count();
      });
    });
    tabModuleSelec[selectPoss] = TabNBModules;
    fullTab[selectPoss] = [countModulesActifs, tabModuleSelec[selectPoss]];
  });
  countModulesActifs = Eole.find({}).count();
  tabModule.forEach((module) => {
    TabNBModules[module] += Eole.find({ module: module }).count();
  });
  fullTab["France Total"] = [countModulesActifs, TabNBModules];

  // Fonction permettant de retourner un tableaux js contenant pour chaque selection possible le nombre de modules actifs afin de les afficher au Hover
  Meteor.methods({
    RequeteStart: function () {
      return fullTab;
    },
  });

  //Function retournant un tableaux js permettant la construction des graphiques au clic sur une académie ou région académique.
  Meteor.methods({
    Requete: function () {
      return [fullTab, tabModule];
    },
  });
});